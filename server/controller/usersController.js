const User = require("../models/userModel");
const bcrypt = require("bcrypt");

module.exports.register = async(req,res,next) => {
    try{
        const { username,email,password } = req.body;
        const usernameCheck = await User.findOne({ username:username });
        const emailCheck = await User.findOne({ email:email });
        if(usernameCheck) return res.json({ status: false ,msg: "Username already used"});
        if(emailCheck) return res.json({ status: false ,msg: "Email already used"});
        const hashedPassword = await bcrypt.hash(password, 10);
        const user = await User.create({
            email,
            username,
            password: hashedPassword
        });
        delete user.password;
        return res.json({ status: true, user});
    } catch(err){
        next(err);
    }
};


module.exports.login = async(req,res,next) => {
    try{
        const { username,password } = req.body;
        const user = await User.findOne({ username:username});
        if(!user)  return res.json({ status: false ,msg: "Incoreect Username or password"});
        const isPasswordValid = await bcrypt.compare(password,user.password);
        if(!isPasswordValid) return res.json({ status: false ,msg: "Incoreect Username or password"});
        delete user.password;
        return res.json({ status: true, user}); 
    } catch(err){
        next(err);
    }
};

module.exports.getAllUsers = async(req,res,next) => {
    try{
        const users = await User.find({_id: { $ne: req.body.id } }).select([
            "email",
            "username",
            "_id"
        ]);
        return res.json(users);
    } catch(err){
        next(err);
    }
};