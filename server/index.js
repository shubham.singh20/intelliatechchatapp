const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const userRoutes = require("./routes/userRoutes");
const messageRoutes = require("./routes/messagesRoutes");
const socket = require("socket.io");

const app = express();


mongoose.connect("mongodb://127.0.0.1:27017/chatApp", { 
    useNewUrlParser: true,
    useUnifiedTopology: true
 });

const con = mongoose.connection;

app.use(cors());
app.use(express.json());

app.use("/api/auth",userRoutes);
app.use("/api/messages",messageRoutes);

con.on('open',() => {
    console.log("DB connected!!!!!!");
});

const server = app.listen(3000, () => {
    console.log("server started!!!!");
});

const io = socket(server, {
    cors: {
        origin: "http://localhost:3000",
        credentials:true,
    }
});

global.onlineUsers = new Map();

io.on("connection", (socket) => {
    global.chatSocket = socket;
    socket.on("add-user", (userId) => {
        onlineUsers.set(userId,socket.id);
    });

    socket.on("send-msg", (data) => {
        const sendUserSocket = onlineUsers.get(data.to);
        if(sendUserSocket){
            socket.to(sendUserSocket).emit("msg-recieve", data.msg);
        }
    });
});